# Body Fat Calculator

This is a very simple bodyfat calculator written with [Jetpack Compose](https://developer.android.com/jetpack/compose).

<img src="./media/bodyfat_calculator_screenshot.png" width=100 style="float:right;width:42px;height:42px;">

## Formula

It uses the Body Tape Navy formula [^1]:

$$
\text{Female:\quad} 163.205 \times \log_{10}\left( w + hp - n \right) - 97.684 \log_{10}\left( h \right) -78.387
$$

$$
\text{Male:\quad} 86.010 \times \log_{10}\left( w-n \right) -70.041 \log_{10}\left( h \right) + 36.76
$$

## Design

All the code is inside [MainActivity.kt](./app/src/main/java/com/example/bodyfatcalculator/MainActivity.kt) and the [this apk](https://gitlab.com/RyanGreenup/body-fat-calculator/-/blob/main/app/release/app-release.apk) is tagged as a [relase](https://gitlab.com/RyanGreenup/body-fat-calculator/-/releases).

## References

---

[^1]: p. 31 of <https://www.armyg1.army.mil/hr/bodyComposition/docs/AR600_9_28-June-2013.pdf> cited in <https://en.wikipedia.org/wiki/Body_fat_percentage#cite_note-24>
