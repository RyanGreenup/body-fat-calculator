package com.example.bodyfatcalculator

// * TODO Add draw that changes what's displayed
// ** TODO Add Boolean Switch for BMI
// ** TODO Include also FFMI

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.bodyfatcalculator.ui.theme.BodyFatCalculatorTheme
import java.lang.Exception
import kotlin.math.log

var i: Int = 0

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BodyFatCalculatorTheme {
                // A surface container using the 'background' color from the theme
                Card {
                    outermainView()
                }
            }
        }
    }
}

@Composable
@Preview
fun outermainView() {

    // Entry Dialogs
    var waist = remember { mutableStateOf(0.0) }
    var hips = remember { mutableStateOf(0.0) }
    var neck = remember { mutableStateOf(0.0) }
    var height = remember { mutableStateOf(0.0) }

    newmainview(height, hips, neck, waist)
}

@Composable
fun newmainview(
    height: MutableState<Double>,
    hips: MutableState<Double>,
    neck: MutableState<Double>,
    waist: MutableState<Double>
) {
    val context = LocalContext.current
    var displayText = remember { mutableStateOf("") }
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        // Display Text
        Text(
            text = displayText.value,
            color = Color.Magenta,
            fontSize = 30.sp,
            fontStyle = FontStyle.Italic
        )

        Spacer(modifier = Modifier.heightIn(60.dp))
//        Spacer(modifier = Modifier.heightIn(60.dp))

        // Set Gender for use with GenderRadioGroup and below entry dialogs
        val radioButtons = listOf(0, 1) // 0 is Male, 1 is Female
        val selectedButton = remember { mutableStateOf(radioButtons.first()) }

        if (selectedButton.value == 1) {

            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {


                Column {
                    numEntryField(waist, context, "Waist: ")
                    numEntryField(neck, context, "Neck: ")
                }
                Spacer(modifier = Modifier.width(15.dp))
                Column {
                    numEntryField(height, context, "Height: ")
                    numEntryField(hips, context, "Hips: ")
                }


                var bf = bodyFat(height.value, hips.value, neck.value, waist.value)

                if (!bf.isNaN()) {
                    if (bf.isInfinite()) {
                        displayText.value = "∞"
                    } else {
                        displayText.value = String.format("%.2f", bf)
                    }

                    if (bf < 0) {
                        Toast.makeText(context, "Careful, This is Negative", Toast.LENGTH_SHORT)
                            .show()
                    }
                }


            }

        } else if (selectedButton.value == 0) {  // 0 is MALE

            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {

                numEntryField(waist, context, "Waist: ")
                numEntryField(neck, context, "Neck: ")
                numEntryField(height, context, "Height: ")

                var bf = bodyFat(height.value, neck.value, waist.value)

                if (!bf.isNaN()) {
                    if (bf.isInfinite()) {
                        displayText.value = "∞"
                    } else {
                        displayText.value = String.format("%.2f", bf)
                    }

                    if (bf < 0) {
                        Toast.makeText(context, "Careful, This is Negative", Toast.LENGTH_SHORT)
                            .show()
                    }
                }


            }

        }

        // Display Radio Button Choice

        Spacer(modifier = Modifier.heightIn(60.dp))

        GenderRadioGroup(selectedButton, radioButtons)


    }
}

@Composable
fun GenderRadioGroup(selectedButton: MutableState<Int>, radioButtons: List<Int>) {

    /*
    Column {
        Row {
            // Male
            Text(text = "Male: ")
            val isSelected = (radioButtons[0] == selectedButton.value)
            RadioButton(selected = isSelected, onClick = {selectedButton.value = radioButtons[0]})
        }
        Row {

            Text(text = "Female")
            val isSelected = (radioButtons[1] == selectedButton.value)
            // Female
            RadioButton(selected = isSelected, onClick = {selectedButton.value = radioButtons[1]})
        }
    }

     */

    Row {
        Column(
            horizontalAlignment = Alignment.End
        ) {

            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start

            ) {
                Text(text = "Male: ")
                // Male
                val isSelected = (radioButtons[0] == selectedButton.value)
                RadioButton(
                    selected = isSelected,
                    onClick = { selectedButton.value = radioButtons[0] })
            }
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start
            ) {
                Text(text = "Female: ")

                val isSelected = (radioButtons[1] == selectedButton.value)
                // Female
                RadioButton(
                    selected = isSelected,
                    onClick = { selectedButton.value = radioButtons[1] })
            }

        }

    }
}

@Composable
fun GenderButton(
    isFemale: MutableState<Boolean>,
    selectedGenderString: MutableState<String>
) {
    if (isFemale.value) {
        selectedGenderString.value = "Female"
    } else {
        selectedGenderString.value = "Male"
    }

    Row {
        Text(text = "Female:      ")
        Switch(
            checked = isFemale.value,
            onCheckedChange = { isFemale.value = it }
        )
    }
}

@Composable
fun GenderRadioGroup(
    radioButtons: List<Int>,
    radioLabels: List<String>,
    selectedButton: MutableState<Int>
) {

    Row(horizontalArrangement = Arrangement.SpaceEvenly) {
        // NOTE a loop hurts readability for only two buttons, avoid here
        // NOTE using a constructing function doesn't help much either, just fucking write it out...
        radioButtons.forEach { index ->
            val isSelected = (index == selectedButton.value)
            Column {
                Text(text = radioLabels[index])
                RadioButton(selected = isSelected, onClick = { selectedButton.value = index })
            }
        }
    }

}


fun bodyFat(h: Double, n: Double, w: Double): Double {
    var h = h / 2.54
    var n = n / 2.54
    var w = w / 2.54

    return 86.01 * log(w - n, 10.0) - 70.041 * log(h, 10.0) + 36.76
}

fun bodyFat(h: Double, hp: Double, n: Double, w: Double): Double {
    var h = h / 2.54
    var hp = hp / 2.54
    var n = n / 2.54
    var w = w / 2.54

    return 163.205 * log(w + hp - n, 10.0) - 97.684 * log(h, 10.0) - 78.387
}

@Composable
fun numEntryField(a: MutableState<Double>, context: Context, desc: String) {
    Column {

//    Text(desc)
        var textVal = remember { mutableStateOf("") }
        OutlinedTextField(
            value = textVal.value,
            onValueChange = {
                textVal.value = it
                if (it != "") {
                    try {
                        a.value = it.toDouble()
                    } catch (ex: Exception) {
                        Toast.makeText(context, "Must Enter Numbers", Toast.LENGTH_SHORT).show()
                    }
                }

            },
            keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
            modifier = Modifier.width(75.dp),
            label = { Text(text = desc) }

        )
    }
}

